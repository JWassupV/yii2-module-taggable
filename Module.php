<?php

namespace jwassupv\taggable;

use Yii;

/**
 * Module [[service]]
 * Yii2 service module.
 */
class Module extends \yii\base\Module {
    public static $name = 'taggable';

}
