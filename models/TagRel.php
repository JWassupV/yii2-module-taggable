<?php

namespace jwassupv\taggable\models;

use Yii;

/**
 * This is the model class for table "{{%tag_rel}}".
 *
 * @property integer $tag_id
 * @property string $model
 * @property integer $model_id
 */
class TagRel extends \jwassupv\base\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tag_rel}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_id', 'model', 'model_id'], 'required'],
            [['tag_id', 'model_id'], 'integer'],
            [['model'], 'string'],
        ];
    }

    public function getTag() {
        return $this->hasOne(Tag::className(), ['id'=>'tag_id'])->from(Tag::tableName().' tag');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag_id' => 'Tag ID',
            'model' => 'Model',
            'model_id' => 'Model ID',
        ];
    }
}
