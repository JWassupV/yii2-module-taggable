<?php
namespace jwassupv\taggable\migrations;

use yii\db\Migration;

class m160706_135554_tbl_tags extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('{{%tag}}', [
            'id'        =>  $this->primaryKey(),
            'frequency' =>  $this->integer(),
            'name'      =>  $this->string(255)
        ],$tableOptions);

        $this->createTable('{{%tag_rel}}', [
            'tag_id'        =>  $this->integer()->unsigned(),
            'model'        =>  $this->string(255),
            'model_id'        =>  $this->integer()->unsigned(),
        ],$tableOptions);
    }

    public function down()
    {
        echo "m160706_135554_tbl_tags cannot be reverted.\n";
        return false;
    }

    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
}
