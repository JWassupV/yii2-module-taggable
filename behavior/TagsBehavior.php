<?php

namespace jwassupv\taggable\behavior;

use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * @author John Vdovin <jonvdovin@gmail.com>
 */
class TagsBehavior extends Behavior
{
    public $modelName;
    
    /**
     * @var string
     */
    public $attribute = 'tagNames';

    /**
     * @var string
     */
    public $title = 'name';

    /**
     * @var string
     */
    public $frequency = 'frequency';
    
    /**
     * @var string
     */
    public $relationName = 'tags';

    /**
     * Tag values
     * @var array|string
     */
    public $tagValues;

    /**
     * @var bool
     */
    public $asArray = false;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true)
    {
        if ($name === $this->attribute) {
            return true;
        }
        return parent::canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        return $this->getTagNames();
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true)
    {
        if ($name === $this->attribute) {
            return true;
        }
        return parent::canSetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        $this->tagValues = $value;
    }
    
    /**
     * @inheritdoc
     */
    private function getTagNames()
    {
        $items = [];
        foreach ($this->owner->{$this->relationName} as $tag) {
            $items[] = $tag->{$this->title};
        }
        return $this->asArray ? $items : implode(',', $items);
    }

    /**
     * @param Event $event
     */
    public function afterSave($event)
    {
        if ($this->tagValues === null) {
            $this->tagValues = $this->owner->{$this->attribute};
        }

        if (!$this->owner->getIsNewRecord()) {
            $this->beforeDelete($event);
        }
        
        $names = array_unique(preg_split(
            '/\s*,\s*/u',
            preg_replace(
                '/\s+/u',
                ' ',
                is_array($this->tagValues)
                ? implode(',', $this->tagValues)
                : $this->tagValues
                ),
            -1,
            PREG_SPLIT_NO_EMPTY
            ));
        $relation = $this->owner->getRelation($this->relationName);
        $pivot = $relation->via->from[0];
        /** @var ActiveRecord $class */
        $class = $relation->modelClass;
        $rows = [];
        $updatedTags = [];
        foreach ($names as $name) {
            $tag = $class::findOne([$this->title => $name]);
            if ($tag === null) {
                $tag = new $class();
                $tag->{$this->title} = $name;
            }
            $tag->{$this->frequency}++;
            if ($tag->save()) {
                $updatedTags[] = $tag;
                $rows[] = ["model_id"=>$this->owner->getPrimaryKey(), 'tag_id'=>$tag->getPrimaryKey(), 'model'=>$this->modelName];
            }
        }
        if (!empty($rows)) {
            $this->owner->getDb()
            ->createCommand()
            ->batchInsert($pivot, [key($relation->via->link), current($relation->link), 'model'], $rows)
            ->execute();
        }

        $this->owner->populateRelation($this->relationName, $updatedTags);
    }

    /**
     * @param Event $event
     */
    public function beforeDelete($event)
    {
        $relation = $this->owner->getRelation($this->relationName);
        $pivot = $relation->via->from[0];
        /** @var ActiveRecord $class */
        $class = $relation->modelClass;
        $query = new Query();
        $pks = $query
            ->select(current($relation->link))
            ->from($pivot)
            ->where([key($relation->via->link) => $this->owner->getPrimaryKey()])
            ->column($this->owner->getDb());
        if (!empty($pks)) {
            $class::updateAllCounters([$this->frequency => -1], ['in', $class::primaryKey(), $pks]);
        }
        $this->owner->getDb()
            ->createCommand()
            ->delete($pivot, [key($relation->via->link) => $this->owner->getPrimaryKey(), 'model'=>  $this->modelName])
            ->execute();
    }
    
    /**
     * @var array
     */
    /*public $rules = [
        [['tagNames'], 'safe']
    ];*/ // added rules to $owner;

    /**
     * @var \yii\validators\Validator[]
     */
//    protected $validators = []; // track references of appended validators

    /**
     * @inheritdoc
     */
    /*public function attach($owner) {
        parent::attach($owner);
        $validators = $owner->validators;
        foreach ($this->rules as $rule) {
            if ($rule instanceof Validator){
                $validators->append($rule);
                $this->validators[] = $rule; // keep a reference in behavior
            } elseif (is_array($rule) && isset($rule[0], $rule[1])) { // attributes, validator type
                $validator = Validator::createValidator($rule[1], $owner, (array) $rule[0], array_slice($rule, 2));
                $validators->append($validator);
                $this->validators[] = $validator; // keep a reference in behavior
            } else {
                throw new InvalidConfigException('Invalid validation rule: a rule must specify both attribute names and validator type.');
            }
        }
    }*/
    
    /**
     * @inheritdoc
     */
    /*public function detach() {
        $ownerValidators = $this->owner->validators;
        $cleanValidators = [];
        foreach ($ownerValidators as $validator) {
            if (!in_array($validator, $this->validators)) {
                $cleanValidators[] = $validator;
            }
        }
        $ownerValidators->exchangeArray($cleanValidators);
    }*/
}