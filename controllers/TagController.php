<?php

namespace jwassupv\taggable\controllers;

use Yii;
use jwassupv\admin\components\Controller;
use jwassupv\taggable\models\Tag;

class TagController extends Controller
{
    public function actionList($query) {
        $models = Tag::find()->filterWhere(['like', 'name', $query])->all();
        $items = [];

        foreach ($models as $model) {
            $items[] = ['name' => $model->name];
        }
        // We know we can use ContentNegotiator filter
        // this way is easier to show you here :)
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $items;
    }
}